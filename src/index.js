import React from "react";
import ReactDOM from "react-dom";
import './styles/index.css';

// Animated numbers
import AnimatedNumber from "animated-number-react";

// Material UI
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    tickerCard: {
        width: '100%',
        marginBottom: '10px',
        padding: '20px',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#192734',
        color: '#ffffff'
    }
}));

const App = () => {
    const classes = useStyles();

    // Kraken
    const [krakenPrice, updateKrakenPrice] = React.useState(0);
    const [krakenConnected, updateKrakenConnected] = React.useState(false);
    const [krakenSubscribed, updateKrakenSubscribed] = React.useState(false);

    // Coinbase
    const [coinbasePrice, updateCoinbasePrice] = React.useState({ price: 0, bid: 0, ask: 0 });
    const [coinbaseConnected, updateCoinbaseConnected] = React.useState(false);
    const [coinbaseSubscribed, updateCoinbaseSubscribed] = React.useState(false);

    // Bitstamp
    const [bitstampPrice, updateBitstampPrice] = React.useState(0);
    const [bitstampConnected, updateBitstampConnected] = React.useState(false);
    const [bitstampSubscribed, updateBitstampSubscribed] = React.useState(false);
    
    // Connect to websocket
    const connectWebSocket = (exchangeName) => {
        let socket;

        // Initalize new websocket
        switch (exchangeName) {
            case 'coinbase':
                socket = new WebSocket('wss://ws-feed.pro.coinbase.com');
                break;
            case 'kraken':
                socket = new WebSocket('wss://ws.kraken.com:443/');
                break;
            case 'bitstamp':
                socket = new WebSocket('wss://ws.bitstamp.net');
                break;
            default:
                throw Error('Invalid exchange name');
        };

        // Connected to websocket
        socket.onopen = () => {
            switch (exchangeName) {
                case 'coinbase':
                    // Show connect success message
                    if (!coinbaseConnected) {
                        console.log('Connected to Coinbase!');
                        updateCoinbaseConnected(true);
                    };

                    // Subscribe to ticker channel
                    if (!coinbaseSubscribed) {
                        console.log('Subscribing to Coinbase ticker channel...');
                        const subscribe = {
                            type: 'subscribe',
                            channels: [{
                                name: 'ticker',
                                product_ids: ['BTC-USD']
                            }]
                        };
                        socket.send(JSON.stringify(subscribe));
                        updateCoinbaseSubscribed(true);
                    };
                    break;

                case 'kraken':
                    // Show connect success message
                    if (!krakenConnected) {
                        console.log('Connected to Kraken!');
                        updateKrakenConnected(true);
                    };

                    // Subscribe to ticker channel
                    if (!krakenSubscribed) {
                        console.log('Subscribing to Kraken ticker channel...');
                        const subscribe = {
                            event: 'subscribe',
                            pair: ['BTC/USD'],
                            subscription: { name: 'ticker' }
                        };
                        socket.send(JSON.stringify(subscribe));
                        updateKrakenSubscribed(true);
                    };
                    break;
                    
                case 'bitstamp':
                    // Show connect success message
                    if (!bitstampConnected) {
                        console.log('Connected to Bitstamp!');
                        updateBitstampConnected(true);
                    };

                    // Subscribe to ticker channel
                    if (!bitstampSubscribed) {
                        console.log('Subscribing to Bitstamp ticker channel...');
                        const subscribe = {
                            event: 'bts:subscribe',
                            data: { channel: 'live_trades_btcusd' }
                        };
                        socket.send(JSON.stringify(subscribe));
                        updateBitstampSubscribed(true);
                    };
                    break;
                default:
                    throw Error('Invalid exchange name');
            };
        };

        // Listen for messages
        socket.onmessage = (event) => {
            const data = JSON.parse(event.data);
            let price;

            switch (exchangeName) {
                case 'coinbase':
                    price = formatNumber(data.price);
                    updateCoinbasePrice(price);
                    break;
                case 'kraken':
                    // Update price & priceList if not heartbeat
                    if (!('event' in data)) {
                        price = parseFloat(parseFloat(data[1].c[0]).toFixed(2));
                        updateKrakenPrice(price);
                    };
                    break;
                case 'bitstamp':
                    if ('price' in data.data) {
                        price = parseFloat(parseFloat(data.data.price).toFixed(2));
                        updateBitstampPrice(price);
                    };
                    break;
                default:
                    throw Error('Invalid exchange name');
            };
        };

        // Connection closed
        socket.onclose = () => {
            console.log(`Disconnected from ${exchangeName}`);
        };
    };

    // Format values for states
    const formatNumber = (value) => parseFloat(parseFloat(value).toFixed(2));

    // Format numbers for display
    const formatDisplayPrice = (price) => {
        const formattedPrice = Number(price).toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        return '$' + formattedPrice;
    };

    // Create websocket connections
    connectWebSocket('kraken');
    connectWebSocket('coinbase');
    connectWebSocket('bitstamp');

    // Create cards
    const exchangeCards = ['coinbase', 'kraken', 'bitstamp'].map((exchange) => {
        let exchangeName;
        let colorClass;
        let price;

        switch (exchange) {
            case 'coinbase':
                exchangeName = 'Coinbase';
                colorClass = 'coinbaseColor';
                price = coinbasePrice;
                break;
            case 'kraken':
                exchangeName = 'Kraken';
                colorClass = 'krakenColor';
                price = krakenPrice;
                break;
            case 'bitstamp':
                exchangeName = 'Bitstamp';
                colorClass = 'bitstampColor';
                price = bitstampPrice;
                break;
            default:
                break;
        };
        
        return (
            <Card key={exchangeName} elevation={2} classes={{ root: classes.tickerCard }}>
                <h1 className={`cardTitle ${colorClass}`}>{exchangeName}</h1>

                <AnimatedNumber
                    className='price'
                    value={price}
                    duration={300}
                    formatValue={(value) => formatDisplayPrice(value)}
                />
            </Card>
        );
    });

    return (
        <section id='container'>
            { exchangeCards }
        </section>
    );
};

ReactDOM.render(<App />, document.querySelector("#root"));