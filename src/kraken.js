import { useState } from 'react';

function Kraken() {
    const [currentPrice, updateCurrentPrice] = useState(0);
    const [hasConnected, updateHasConnected] = useState(false);
    const [hasSubscribed, updateHasSubscribed] = useState(false);

    const socket = new WebSocket('wss://ws.kraken.com:443/');

    // Connected
    socket.onopen = () => {
        if (!hasConnected) {
            console.log('Connected to Kraken!');
            updateHasConnected(true);
        };

        // Send subscribe message to Kraken
        if (!hasSubscribed) {
            console.log('Subscribing to BTC/USD ticker');
            
            // Subscribe to ticker
            const subscribe = {
                event: 'subscribe',
                pair: ['BTC/USD'],
                subscription: { name: 'ticker' }
            };
            socket.send(JSON.stringify(subscribe));
            updateHasSubscribed(true);
        };
    };

    // Listen for messages
    socket.onmessage = (event) => {
        const data = JSON.parse(event.data);
        
        // Update price if not heartbeat
        if (!('event' in data)) {
            const price = parseFloat(data[1].c[0]).toFixed(2);
            updateCurrentPrice(price);
        };
    };

    // Connection closed
    socket.onclose = () => {
        console.log('Disconnected from Kraken');
    };

    return currentPrice;
};

export default Kraken;

